require "spec_helper"

describe Extractor do
  describe "Empty arguments" do
    context "raise error" do
      it "- Empty logfile" do
        expect { Extractor.new nil, 'InitGame' }.to raise_error(ArgumentError)
      end

      it "- Empty key_match" do
        expect { Extractor.new 'Some Path', nil }.to raise_error(ArgumentError)
      end
     end
  end

  describe "Valid arguments" do
    let(:logfile) { './spec/tmp/short_game.log' }

    context "InitGame as Key" do
      subject(:initgame_subj) { Extractor.new logfile, 'InitGame' }

      it { expect(initgame_subj.execute_extraction).not_to be_empty }
      it { expect(initgame_subj.execute_extraction.size).to eq(1) }
    end

    context "Kill as Key" do
      subject(:kill_subj) { Extractor.new logfile, 'Kill' }

      it { expect(kill_subj.execute_extraction).not_to be_empty }
      it { expect(kill_subj.execute_extraction.size).to eq(11) }
    end

    context "Item as Key" do
      subject(:item_subj) { Extractor.new logfile, 'Item' }

      it { expect(item_subj.execute_extraction).not_to be_empty }
      it { expect(item_subj.execute_extraction.size).to eq(61) }
    end

    context "LikeIt as Key" do
      subject(:likeit_subj) { Extractor.new logfile, 'LikeIt' }

      it { expect(likeit_subj.execute_extraction).to be_empty }
    end

    context "Given more keys to search" do
      subject(:two_keys) { Extractor.new logfile, 'InitGame, Kill' }

      it { expect(two_keys.execute_extraction.size).to eq(12) }
    end
  end
end
