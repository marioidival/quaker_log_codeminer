require "spec_helper"

describe Report do
  def loglist locate_log
    extractor = Extractor.new locate_log, 'InitGame, Kill'
    extractor.execute_extraction
  end

  describe "Empty arguments" do
    context "raise error" do
      it "- Empty loglist" do
        expect { Report.new }.to raise_error(ArgumentError)
      end
    end
  end

  describe "Make report of short game" do
    subject(:loglist_f) { loglist './spec/tmp/short_game.log' }
    subject(:game_report) { Report.new loglist_f }

    describe "report details" do
      subject(:short_game) { game_report.make_report }

      context "total games" do
        it { expect(short_game.size).to eq(1) }
      end

      context "total kills" do
        it { expect(short_game["game_1"]["total_kills"]).to eq(11) }
      end

      context "players" do
        it { expect(short_game["game_1"]["players"]).to include("Isgalamido", "Mocinha") }
      end

      describe "rank kills" do
        context "kills Isgalamido" do
          it { expect(short_game["game_1"]["kills"]["Isgalamido"]).to eq(-9) }
        end

        context "kills Mocinha" do
          it { expect(short_game["game_1"]["kills"]["Mocinha"]).to eq(-1) }
        end
      end
    end
  end

  describe "General ranked kill" do
    subject(:loglist_f) { loglist './spec/tmp/medium_games.log' }
    subject(:game_report) { Report.new loglist_f }

    describe "Report details - medium games" do
      subject(:medium_games) { game_report.make_report }

      context "total games" do
        it { expect(medium_games.size).to eq(2) }
      end
    end

    describe "Rank kills of medium games log" do
      subject(:ranked) { game_report.general_rank }

      context "total players" do
        it { expect(ranked.size).to eq(4) }
      end

      context "Godlike player" do
        it { expect(ranked.max_by{|player, kills| kills}).to eq(["Isgalamido", 29]) }
      end

      context "Newbee player" do
        it { expect(ranked.min_by{|player, kills| kills}).to eq(["Dono da Bola", 16]) }
      end

      context "Total kills of medium games log" do
        it { expect(ranked.map{|player, kill| kill}.reduce(:+)).to eq(87) }
      end
    end
  end
end
