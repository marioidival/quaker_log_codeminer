# TestQuakerLog

Quaker Log test to Codeminer42 - TEste

## Usage

#### Report Game

	ruby bin/quaker_log report_game

or Report game to specific file:

	ruby bin/quaker_log report_game path/to/file

By default, lib use games.log of gist


#### Killer Rank

	ruby bin/quaker_log general_ranked

or to specific file:

	ruby bin/quaker_log general_ranked path/to/file


#### Plus

	ruby bin/quaker_log plus

or to specific file:

	ruby bin/quaker_log plus path/to/file


## Tests

	rspec

## Test ngrok
