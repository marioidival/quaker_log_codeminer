require 'json'

class Report
  attr_reader :general_ranked, :means_of_death

  def initialize loglist
    raise "insert a loglist" if loglist.nil?

    @loglist = loglist
    @general_rank_h = {}
    @means_of_death_h = {}
  end

  def create_hash_report current_game, hash_report
    hash_report.store(current_game, {})
    hash_report[current_game].store("total_kills", 0)
    hash_report[current_game].store("players", [])
    hash_report[current_game].store("kills", {})

    hash_report
  end

  def make_report
    game_number = 1
    hash_report = {}

    for line in @loglist
      lmatch = line.match(/InitGame:/)

      if lmatch
        current_game = "game_#{game_number}"
        @means_of_death_h.store(current_game, {})

        hash_report = create_hash_report current_game, hash_report
        game_number += 1
      end

      kmatch = line.match(/Kill/)

      unless kmatch.nil?
        hash_report[current_game]["total_kills"] += 1
        game = hash_report[current_game]

        players_in_game = game["players"]
        killers = game["kills"]
        match_r = line.match(/. Kill: \d+ \d+ \d+:\s(.*)\skilled\s(.*)\sby\s(.*)/)

        killer = match_r[1]
        deceassed = match_r[2]
        means = match_r[3]

        unless @means_of_death_h[current_game].key? means
          @means_of_death_h[current_game].store(means, 0)
        end

        @means_of_death_h[current_game][means] += 1

        unless killer == "<world>"
          unless players_in_game.include? killer
            players_in_game << killer
          end

          unless killers.key? killer
            killers[killer] = 0
          end

          unless @general_rank_h.key? killer
            @general_rank_h.store(killer, 0)
          end

          if killer != deceassed
            killers[killer] += 1
            @general_rank_h[killer] += 1
          end
        end

        unless players_in_game.include? deceassed
          players_in_game << deceassed
        end

        unless killers.key? deceassed
          killers[deceassed] = 0
        end

        killers[deceassed] -= 1
      end
    end

    hash_report
  end

  def general_rank
    self.make_report
    @general_rank_h.sort_by { |player, kills| kills }.reverse.to_h
  end

  def means_of_death
    self.make_report
    @means_of_death_h
  end
end
