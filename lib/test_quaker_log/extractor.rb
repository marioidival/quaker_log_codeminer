class Extractor
  def initialize logfile, key_match
    raise ArgumentError, "No logfile???" if logfile.nil?
    raise ArgumentError, "No key match to search in file???" if key_match.nil?

    @logfile = logfile
    @key = key_match
  end

  def execute_extraction
    file = open(@logfile)

    # generate key like KEY_ONE|KEY_TWO
    to_search = @key.split(',').join('|')

    # Extract lines if line containes @key
    file.readlines.select { |line| line =~ /#{to_search}/ }
  end
end
